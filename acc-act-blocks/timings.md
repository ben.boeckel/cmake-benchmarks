On Linux, 3.11.0-rc1 was used. On Windows, commit
57f35bba84f53300641c6a597b4eca728f02874a which is after rc2, but will be
included in rc3. This is due to a performance regression found in rc1 in the
course of these timings.

# VTK + Java + Python

Using VTK 4fbc4a6c0311673e47fb05f27d0e98e3f9e3abc7 (`master` as of 2 Mar 2018).

## 3.10.2

Linux + Ninja:
  Time (mean ± σ):     205.911 s ±  3.043 s    [User: 189.349 s, System: 14.451 s]
  Range (min … max):   201.285 s … 210.618 s

Windows + Ninja:
  Time (mean ± σ):     85.746 s ±  0.730 s    [User: 2.8 ms, System: 5.1 ms]
  Range (min … max):   84.191 s … 86.734 s

VS2017:
  Time (mean ± σ):    169.088 s ±  0.789 s    [User: 2.9 ms, System: 2.8 ms]
  Range (min … max):  167.819 s … 170.471 s

## 3.11.0

Linux + Ninja:
  Time (mean ± σ):     24.686 s ±  0.451 s    [User: 20.356 s, System: 4.038 s]
  Range (min … max):   24.378 s … 25.883 s

Windows + Ninja:
  Time (mean ± σ):     65.875 s ±  2.002 s    [User: 0.0 ms, System: 5.3 ms]
  Range (min … max):   64.456 s … 71.350 s

VS2017:
  Time (mean ± σ):    112.047 s ±  1.262 s    [User: 1.4 ms, System: 6.6 ms]
  Range (min … max):  110.042 s … 113.729 s

Using VTK's new module system as of cf8d2a361712bcf387fb4eaf33e232a224470d7e.

## 3.10.2

Linux + Ninja:
  Time (mean ± σ):     205.911 s ±  3.043 s    [User: 189.349 s, System: 14.451 s]
  Range (min … max):   201.285 s … 210.618 s

Windows + Ninja:
  Time (mean ± σ):    321.195 s ±  4.217 s    [User: 1.4 ms, System: 2.6 ms]
  Range (min … max):  315.577 s … 330.355 s

VS2017:
  Time (mean ± σ):    252.451 s ±  2.852 s    [User: 0.0 ms, System: 5.2 ms]
  Range (min … max):  249.216 s … 258.651 s

## 3.11.0

Linux + Ninja:
  Time (mean ± σ):     20.216 s ±  0.054 s    [User: 17.057 s, System: 3.095 s]
  Range (min … max):   20.152 s … 20.339 s

Windows + Ninja:
  Time (mean ± σ):     74.930 s ±  1.364 s    [User: 1.4 ms, System: 4.0 ms]
  Range (min … max):   73.608 s … 78.522 s

VS2017:
  Time (mean ± σ):     97.973 s ±  1.744 s    [User: 4.4 ms, System: 2.6 ms]
  Range (min … max):   95.543 s … 100.247 s

# Benchmark project

## 500 chunks 10 commands

### 3.10.2

Linux + Ninja:
  Time (mean ± σ):      5.565 s ±  0.185 s    [User: 5.162 s, System: 0.309 s]
  Range (min … max):    5.206 s …  5.843 s

Windows + Ninja:
  Time (mean ± σ):      6.881 s ±  0.048 s    [User: 2.9 ms, System: 1.2 ms]
  Range (min … max):    6.814 s …  6.954 s

VS2017:
  Time (mean ± σ):     16.261 s ±  0.180 s    [User: 0.0 ms, System: 5.2 ms]
  Range (min … max):   16.102 s … 16.746 s

### 3.11.0

Linux + Ninja:
  Time (mean ± σ):      1.169 s ±  0.008 s    [User: 1.009 s, System: 0.142 s]
  Range (min … max):    1.159 s …  1.189 s

Windows + Ninja:
  Time (mean ± σ):      3.914 s ±  0.041 s    [User: 1.5 ms, System: 6.4 ms]
  Range (min … max):    3.850 s …  3.992 s

VS2017:
  Time (mean ± σ):     11.600 s ±  1.199 s    [User: 1.5 ms, System: 4.1 ms]
  Range (min … max):   11.031 s … 14.954 s

## 1000 chunks 10 commands

### 3.10.2

Linux + Ninja:
  Time (mean ± σ):     24.501 s ±  0.342 s    [User: 23.383 s, System: 0.815 s]
  Range (min … max):   24.158 s … 25.110 s

Windows + Ninja:
  Time (mean ± σ):     28.188 s ±  0.159 s    [User: 4.4 ms, System: 2.7 ms]
  Range (min … max):   27.991 s … 28.484 s

VS2017:
  Time (mean ± σ):     51.697 s ±  0.337 s    [User: 1.5 ms, System: 5.4 ms]
  Range (min … max):   51.378 s … 52.560 s

### 3.11.0

Linux + Ninja:
  Time (mean ± σ):      2.816 s ±  0.011 s    [User: 2.406 s, System: 0.359 s]
  Range (min … max):    2.796 s …  2.828 s

Windows + Ninja:
  Time (mean ± σ):      7.704 s ±  0.048 s    [User: 1.5 ms, System: 0.0 ms]
  Range (min … max):    7.640 s …  7.801 s

VS2017:
  Time (mean ± σ):     23.024 s ±  0.523 s    [User: 0.0 ms, System: 0.0 ms]
  Range (min … max):   22.582 s … 24.131 s

## 2500 chunks 10 commands

### 3.10.2

Linux + Ninja:
  Time (mean ± σ):     166.732 s ±  3.986 s    [User: 161.817 s, System: 3.146 s]
  Range (min … max):   161.180 s … 171.677 s

Windows + Ninja:
  Time (mean ± σ):     187.736 s ±  0.299 s    [User: 2.9 ms, System: 5.0 ms]
  Range (min … max):   187.167 s …  188.099 s

### 3.11.0

Linux + Ninja:
  Time (mean ± σ):     10.851 s ±  0.222 s    [User: 8.975 s, System: 1.415 s]
  Range (min … max):   10.706 s … 11.331 s

Windows + Ninja:
  Time (mean ± σ):     19.268 s ±  0.116 s    [User: 0.0 ms, System: 3.8 ms]
  Range (min … max):   19.124 s … 19.449 s

VS2017:
  Time (mean ± σ):     58.722 s ±  0.789 s    [User: 1.5 ms, System: 3.8 ms]
  Range (min … max):   57.997 s … 60.827 s
